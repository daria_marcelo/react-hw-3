import React from 'react';
import PropTypes from 'prop-types';
import "./Button.scss";

const Button = ({ optionalClassName, backgroundColor, text, onClick }) => {
    return (
        <button
            className={`btn ${optionalClassName}`}
            style={{ backgroundColor }}
            onClick={onClick}
        >
            {text}
        </button>
    );
};

Button.defaultProps = {
    type: 'button',
    onClick: () => {}
};

Button.propTypes = {
    optionalClassName: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func
};

export default Button;
