import React from 'react';
import PropTypes from 'prop-types';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './ProductCard.scss';

const ProductCard = ({
  product,
  inCart,
  inFavorites,
  AddToCart,
  RemoveFromCart,
  AddToFavorites,
  showCartModal,
  cartItems,
}) => {
  const { id, name, price, image } = product;

  const handleClickAddToCart = () => {
    AddToCart(product);
  };

  const handleClickRemoveFromCart = () => {
    RemoveFromCart(product.id);
  };

  const handleClickAddToFavorites = () => {
    AddToFavorites(product);
  };

  const handleToggleCartModal = () => {
    showCartModal(product);
  };

  return (
    <div className="product-card">
      <div className="image-wrapper">
        <img src={image} alt={name} />
      </div>
      <h3 className="product-name">{name}</h3>

      <div className="card-footer">
        {!inFavorites && (
          <button className="add-favorite" onClick={handleClickAddToFavorites}>
            <FontAwesomeIcon icon={faStar} size="xs" style={{ color: 'blue' }} />
          </button>
        )}
        {inFavorites && (
          <button
            className="add-favorite"
            style={{ backgroundColor: '#dddddd', borderRadius: '60px' }}
            onClick={handleClickAddToFavorites}
          >
            <FontAwesomeIcon icon={faStar} size="xs" style={{ color: 'yellow' }} />
          </button>
        )}
        <p className="product-price">{price} грн</p>
      </div>

      {!inCart && <button className="add-to-cart" onClick={handleClickAddToCart}>Додати до кошика</button>}
      {inCart && (
        <div className="add-remove-btn__wrapper">
          <button className="btn minus-from-cart" onClick={handleClickRemoveFromCart}>-</button>
          <div className="quantity-product">
            {cartItems?.find(item => item.id === id)?.quantity}
          </div>
          <button className="btn plus-to-cart" onClick={handleClickAddToCart}>+</button>
        </div>
      )}
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object,
  inCart: PropTypes.bool,
  inFavorites: PropTypes.bool,
  AddToCart: PropTypes.func,
  RemoveFromCart: PropTypes.func,
  AddToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
};

export default ProductCard;
