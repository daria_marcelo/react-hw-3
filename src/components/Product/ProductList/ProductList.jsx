import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import './ProductList.scss';


const ProductList = ({
  products,
  cartItems,
  favoriteItems,
  onRemoveFromCart,
  onAddToCart,
  onAddToFavorites,
  showCartModal,
}) => {
  return (
    <div className="container">
      <div className="product-list">
        {products.map(product =>
          <ProductCard
            key={product.id}
            product={product}
            inCart={cartItems.some(item => item.id === product.id)}
            inFavorites={favoriteItems.some(item => item.id === product.id)}
            AddToCart={onAddToCart}
            RemoveFromCart={onRemoveFromCart}
            AddToFavorites={onAddToFavorites}
            showCartModal={showCartModal}
            cartItems={cartItems}
          />,
        )}
      </div>

    </div>
  );
};

ProductList.propTypes = {
  products: PropTypes.array,
  cartItems: PropTypes.array,
  favoriteItems: PropTypes.array,
  AddToCart: PropTypes.func,
  RemoveFromCart: PropTypes.func,
  AddToFavorites: PropTypes.func,
};

export default ProductList;
