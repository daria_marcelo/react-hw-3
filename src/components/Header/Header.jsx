import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping, faStar } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import './Header.scss';

export default function Header({ cartItems, favoriteItems }) {
  const cartItemTotal = cartItems.reduce((total, item) => total + item.quantity, 0);
  const favoriteItemsTotal = favoriteItems.length;

  return (
    <header className="header">
      <div className="container header-wrapper">
        <Link to={'/'} className={'header__link'}>
          <h1 className="title">World of Funko POP!</h1>
        </Link>
        <div className="icons-wrapper">
          <div className="cart-shopping-wrapper">
            <Link to={'cart'} className={'header__link'}>
              <FontAwesomeIcon
                icon={faCartShopping}
                size="sm"
                style={{ color: cartItems.length ? 'yellow' : 'blue' }}
              />
              <span>{cartItemTotal}</span>
            </Link>
          </div>
          <div className="star-wrapper">
            <Link to={'favourite'} className={'header__link'}>
              <FontAwesomeIcon
                icon={faStar}
                size="sm"
                style={{ outlineColor: 'blue', color: favoriteItems.length ? 'yellow' : 'blue' }}
              />
              <span>{favoriteItemsTotal}</span>
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
}
