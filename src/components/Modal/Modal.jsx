import React from "react";
import PropTypes from 'prop-types';
import "./Modal.scss";


const Modal = ({
  header,
  optionalClassName,
  closeButton,
  totalPrice,
  text,
  actions,
  closeModal,
  handleOutsideClick
}) => {

  const onInsideClick = (event) => {
    event.stopPropagation();
  }

  return (
    <div className="modal-wrapper" onClick={handleOutsideClick}>
      <div className={`modal ${optionalClassName}`} onClick={onInsideClick}>
        <div className="modal-content">
          <div className="modal-header">
            <h2>{header}</h2>
            {closeButton && (
              <span className="modal-close" onClick={closeModal}>
                &times;
              </span>
            )}
          </div>
          <div className="modal-body">
            <p className="body-text">{text}</p>
          </div>
          <div className="modal-footer">{actions}</div>
          {totalPrice}
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.element,
  closeModal: PropTypes.func,
  handleOutsideClick: PropTypes.func,
};

export default Modal;
