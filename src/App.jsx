import React, { useState } from 'react';
import Modal from './components/Modal';
import Header from './components/Header';
import './App.scss';
import { Outlet } from 'react-router-dom';

const App = () => {
  const [cartItems, setCartItems] = useState(JSON.parse(localStorage.getItem('cartItems')) || []);
  const [favoriteItems, setFavoriteItems] = useState(JSON.parse(localStorage.getItem('favoriteItems')) || []);


  const onAddToCart = (product) => {
    const existingItemIndex = cartItems.findIndex(item => item.id === product.id);
    let updatedCartItems = [...cartItems];

    if (existingItemIndex === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
        quantity: 1,
      };
      updatedCartItems.push(newItem);
    } else {
      const updatedItem = {
        ...cartItems[existingItemIndex],
        quantity: cartItems[existingItemIndex].quantity + 1,
      };
      updatedCartItems.splice(existingItemIndex, 1, updatedItem);
    }

    setCartItems(updatedCartItems);
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
  };

  const onRemoveFromCart = (id) => {
    let updatedCartItems;

    const itemToEdit = cartItems.find((item) => item.id === id);

    if (itemToEdit.quantity === 1) {
      updatedCartItems = cartItems.filter(item => item.id !== id);
    } else {
      const updatedItem = {
        ...itemToEdit,
        quantity: itemToEdit.quantity - 1,
      };

      updatedCartItems = cartItems.map((item) => item.id === id ? updatedItem : item);
    }

    setCartItems(updatedCartItems);
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
  };


  const onAddToFavorites = (product) => {
    const existingItemIndex = favoriteItems.findIndex(item => item.id === product.id);
    let updatedFavouriteItems;

    if (existingItemIndex === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
      };
      updatedFavouriteItems = [...favoriteItems, newItem];
    } else {
      updatedFavouriteItems = favoriteItems.filter(item => item.id !== product.id);
    }

    setFavoriteItems(updatedFavouriteItems);
    localStorage.setItem('favoriteItems', JSON.stringify(updatedFavouriteItems));
  };

  const routerContext = {
    cartItems,
    favoriteItems,
    onAddToCart,
    onRemoveFromCart,
    onAddToFavorites,
  };

  return (
    <>
      <div className="app-wrapper">
        <Header cartItems={cartItems} favoriteItems={favoriteItems} />
        <Outlet className={'page-content'} context={routerContext} />

      </div>
    </>
  );
};

export default App;
