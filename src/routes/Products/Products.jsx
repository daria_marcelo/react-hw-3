import ProductList from '../../components/Product/ProductList';
import React, { useEffect, useState } from 'react';
import { useOutletContext } from 'react-router-dom';

export default function Products() {
  const {
    cartItems,
    favoriteItems,
    onAddToCart,
    onRemoveFromCart,
    onAddToFavorites,
  } = useOutletContext();

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3000/products.json')
      .then(response => response.json())
      .then(products => {
        setProducts(products);
      })
      .catch(error => {
        console.error('Error fetching products:', error);
      });
  }, []);

  return (
    <ProductList
      products={products}
      cartItems={cartItems}
      favoriteItems={favoriteItems}
      onAddToCart={onAddToCart}
      onRemoveFromCart={onRemoveFromCart}
      onAddToFavorites={onAddToFavorites}
      // showCartModal={handleToggleCartModal}
    />
  );
}
