import React from 'react';
import Cart from './Cart/Cart';
import Favourite from './Favourite/Favourite';
import Products from './Products/Products';

export default [
  {
    path: '/',
    element: <Products />,
  },
  {
    path: '/cart',
    element: <Cart />,
  },
  {
    path: '/favourite',
    element: <Favourite />,
  },
];
