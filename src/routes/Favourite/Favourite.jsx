import React from 'react';
import { useOutletContext } from 'react-router-dom';
import './Favourite.scss';

export default function Favourite() {
  const { favoriteItems, onAddToCart } = useOutletContext();

  return (
    <div className={'favourite'}>
      {!favoriteItems.length ? <h1>У вас ще немає улюблених товарів.</h1> : ''}
      {favoriteItems.map(item =>
        <div className="favourite__wrapper" key={item.id}>
          <div className="favourite__name">{item.name} - {item.price} грн</div>
          <button className={`btn-secondary`} onClick={() => onAddToCart(item)}>Додати в корзину</button>
        </div>,
      )}
    </div>
  );
}
