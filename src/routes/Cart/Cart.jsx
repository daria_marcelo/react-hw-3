import React, { useState } from 'react';
import { useOutletContext } from 'react-router-dom';
import './Cart.scss';
import '../../components/Button'
import Modal from "../../components/Modal";

export default function Cart() {
  const { cartItems, onRemoveFromCart } = useOutletContext();
  const [isShowDeleteModal, setShowDeleteModal] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);

  const onRemoveItem = (itemId) => {
    const itemToDelete = Array.isArray(cartItems)
      ? cartItems.find((item) => item.id === itemId)
      : null;

    if (!itemToDelete) {
      console.error(`No item ${itemId} found in cartItems`);
      return;
    }

    setItemToDelete(itemToDelete);
    setShowDeleteModal(true);
  }

  const onCloseModal = () => {
    setItemToDelete(null);
    setShowDeleteModal(false);
  }

  const acceptRemoveItem = () => {
    onRemoveFromCart(itemToDelete.id);
    onCloseModal();
  }


  const totalPrice = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);

  return (
    <div className={'cart'}>
      {cartItems.map(
        item =>
          <div className="cart__wrapper" key={item.id}>
            <div className="cart__name">{item.name} ({item.quantity} x {item.price}грн)</div>
            <button className={`btn-secondary`} onClick={() => onRemoveItem(item.id)}>Видалити</button>
          </div>,
      )}

      {isShowDeleteModal === true &&
        <Modal
          header={'Підтвердіть видалення'}
          handleOutsideClick={onCloseModal}
          closeButton={true}
          closeModal={onCloseModal}
          text={`Ви впевнені, що хочете видалити ${itemToDelete.name} з кошика?`}
          actions={
            <div className={`btn-wrapper`}>
              <button className={'btn btn-primary'} onClick={acceptRemoveItem}>
                Підтвердити видалення
              </button>
              <button className={`btn btn-secondary`} onClick={() => onCloseModal()}>
                Скасувати видалення
              </button>
            </div>
          }
        />
      }
      {cartItems.length
          ? <div className="cart__total">Разом: {totalPrice} грн</div>
          : <h1>Ваш кошик порожній.</h1>
      }
    </div>
  );
}
